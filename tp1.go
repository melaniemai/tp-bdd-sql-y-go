package main

import(
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"log"

)

type Consumo struct{
	Nrotarjeta string
	Cod string
	Nrocomercio string
	Monto string
}


func cargarSPyTriggers(){

	crearInsertarCierres()
	crearAutorizacionDeCompra()
	crearSeAgregoUnaAlerta()
	crearComprasTiempo()
	crearGenerarResumen()
	crearTriggers()
		
}



func obtenerConsumos() []Consumo {
	
	db,err := sql.Open("postgres", "user=postgres host=localhost dbname=tp1 sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	
	var consumos []Consumo
	rows,err := db.Query(`select * from consumo;`)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()
	var c Consumo;
	for rows.Next(){
		if err := rows.Scan(&c.Nrotarjeta, &c.Cod, &c.Nrocomercio, &c.Monto);err != nil {
			log.Fatal(err)
		}
		consumos = append(consumos,c)
		
	}
	if err = rows.Err(); err!=nil {
		log.Fatal(err)
	}
	return consumos
	
}
			
func probarConsumos() {
		db,err := sql.Open("postgres", "user=postgres host=localhost dbname=tp1 sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	consumos:= obtenerConsumos()
	for _, c := range consumos {
		ok := autorizacionDeCompra(c.Nrotarjeta, c.Cod, c.Nrocomercio, c.Monto)
		if ok {
			fmt.Println("Se autorizo la compra de la tarjeta: ", c.Nrotarjeta)
		}else {
			fmt.Println("Se rechazo la compra de la tarjeta: ", c.Nrotarjeta)
		}
	}
	
} 

func autorizacionDeCompra(nrotarjeta string, cod string, numero string, monto string) bool{
	
	db,err := sql.Open("postgres", "user=postgres host=localhost dbname=tp1 sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	ret:= false
	
	rows,err := db.Query(`select * from  autorizacionDeCompra('`+nrotarjeta+`','`+cod+`',`+numero+`,`+monto+`);`)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	for rows.Next() {
		if err := rows.Scan(&ret); err != nil {
			log.Fatal(err)
		}
	}
	if err = rows.Err(); err!= nil {
		log.Fatal(err)
	}
	return ret
}

func generarResumen(){

	db,err := sql.Open("postgres", "user=postgres host=localhost dbname=tp1 sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	_, err = db.Query(`
		select cargarCierres();
		select generarresumen(1, 11);
		select generarresumen(11,11);
		select generarresumen(4, 11);
		select generarresumen(14,11);
		select generarresumen(19,11);
		select generarresumen(20,11);

	`)
}

func cargarBoltDB(){
	crearBaseConDatos()
}

func mostrarMenu() {

		fmt.Println("***BASE DE DATOS***")
		fmt.Println("1. Crear base de datos")
		fmt.Println("2. Crear tablas")
		fmt.Println("3. Crear PK's y FK's")
		fmt.Println("4. Eliminar PK's y FK's")
		fmt.Println("5. Cargar tablas")
		fmt.Println("6. Cargar store procedures y triggers")
		fmt.Println("7. Probar consumos")
		fmt.Println("8. Generar resumen >:(")
		fmt.Println("9. Cargar datos noSQL en BoltBD")
		fmt.Println("0. Salir")
		fmt.Printf("Elegir opcion: ")
}


func main(){
	db,err := sql.Open("postgres", "user=postgres host=localhost dbname=postgres sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	
	_,err = db.Exec(`drop database if exists tp1;`)
	if err != nil {
		log.Fatal(err)
	}
	
	manejadorMenu();	
}


func manejadorMenu(){
	var flag bool
	flag = true
		
	for flag {
		mostrarMenu()
		var solicitudUsuario int	
		fmt.Scanf("%d", &solicitudUsuario)
		switch solicitudUsuario {
			case 1: 
				crearBaseDeDatos()
				fmt.Println("BASE DE DATOS CREADA\n")
			case 2: 
				crearTablas()
				fmt.Println("CREANDO TABLAS!\n")
			case 3: 
				cargarKeys()
				fmt.Println("PK's Y FK's CREADAS :D\n")
			case 4: 
				eliminarKeys()
				fmt.Println("PK's Y FK's ELIMINADAS :(\n")
			case 5: 
				cargarTablas()
				fmt.Println("CARGANDO TABLAS!!\n")
			case 6: 
				cargarSPyTriggers()
				fmt.Println("CARGANDO TRIGGERS Y SP!!\n")
			case 7: 
				probarConsumos()
				fmt.Println("PROBANDO CONSUMOS!!\n")
			case 8: 
				generarResumen()
				fmt.Println("GENERANDO RESUMEN!!\n")
			case 9: 
				cargarBoltDB()
			case 0: 
				fmt.Println("Adios! Vuelva pronto :)\n")
				flag = false
			default: 
				fmt.Println(solicitudUsuario, "NO es una opcion valida!!! :( \nIntenta de nuevo!\n")
		}
	}
}
