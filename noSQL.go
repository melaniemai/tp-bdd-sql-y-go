package main

import (
	"encoding/json"
	"fmt"
	bolt "go.etcd.io/bbolt"
	"log"
	"strconv"
)

type Cliente struct {
	Nrocliente int
	Nombre     string
	Apellido   string
	Domicilio  string
	Telefono   string
}

type Tarjeta struct {
	Nrotarjeta   int
	Nrocliente   int
	Validadesde  string
	Validahasta  string
	Codseguridad int
	Limitecompra int
	Estado       string
}

type Comercio struct {
	Nrocomercio  int
	Nombre       string
	Domicilio    string
	Codigopostal string
	Telefono     string
}

type Compra struct {
	Nrooperacion int
	Nrotarjeta   int
	Nrocomercio  int
	Fecha        string
	Monto        int
	Pagado       bool
}

func CreateUpdate(db *bolt.DB, bucketName string, key []byte, val []byte) error {
	tx, err := db.Begin(true)
	if err != nil {
		return err
	}
	defer tx.Rollback()

	b, _ := tx.CreateBucketIfNotExists([]byte(bucketName))

	err = b.Put(key, val)
	if err != nil {
		return err
	}

	// cierra transaccion
	if err := tx.Commit(); err != nil {
		return err
	}

	return nil
}

func ReadUnique(db *bolt.DB, bucketName string, key []byte) ([]byte, error) {
	var buf []byte

	//abre una transaccion de lectura
	err := db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(bucketName))
		buf = b.Get(key)
		return nil
	})
	return buf, err
}
func crearBaseConDatos() {
	db, err := bolt.Open("datos.db", 0600, nil)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	//cliente 1
	cliente := Cliente{1, "Ezequiel", "Mosqueira", "Reconquista 1782", "11-3654-1245"}
	data, err := json.Marshal(cliente)
	if err != nil {
		log.Fatal(err)
	}
	CreateUpdate(db, "cliente", []byte(strconv.Itoa(cliente.Nrocliente)), data)
	resultado, err := ReadUnique(db, "cliente", []byte(strconv.Itoa(cliente.Nrocliente)))

	fmt.Printf("%s\n", resultado)
	//cliente 2
	cliente = Cliente{2, "Claudia", "Gimenez", "Cabildo 4058", "11-7821-9942"}
	data, err = json.Marshal(cliente)
	if err != nil {
		log.Fatal(err)
	}
	CreateUpdate(db, "cliente", []byte(strconv.Itoa(cliente.Nrocliente)), data)
	resultado, err = ReadUnique(db, "cliente", []byte(strconv.Itoa(cliente.Nrocliente)))

	fmt.Printf("%s\n", resultado)
	//cliente 3
	cliente = Cliente{3, "Nicolas", "Peralta", "Pilcomayo 331", "11-6548-1514"}
	data, err = json.Marshal(cliente)
	if err != nil {
		log.Fatal(err)
	}
	CreateUpdate(db, "cliente", []byte(strconv.Itoa(cliente.Nrocliente)), data)
	resultado, err = ReadUnique(db, "cliente", []byte(strconv.Itoa(cliente.Nrocliente)))

	fmt.Printf("%s\n", resultado)

	//tarjeta1
	tarjeta := Tarjeta{4154830586255180, 1, "201804", "202304", 1912, 45000, "vigente"}
	data, err = json.Marshal(tarjeta)
	if err != nil {
		log.Fatal(err)
	}
	CreateUpdate(db, "tarjeta", []byte(strconv.Itoa(tarjeta.Nrotarjeta)), data)
	resultado, err = ReadUnique(db, "tarjeta", []byte(strconv.Itoa(tarjeta.Nrotarjeta)))

	fmt.Printf("%s\n", resultado)

	//tarjeta2
	tarjeta = Tarjeta{4074683476639311, 2, "201903", "202403", 1423, 30000, "vigente"}
	data, err = json.Marshal(tarjeta)
	if err != nil {
		log.Fatal(err)
	}
	CreateUpdate(db, "tarjeta", []byte(strconv.Itoa(tarjeta.Nrotarjeta)), data)
	resultado, err = ReadUnique(db, "tarjeta", []byte(strconv.Itoa(tarjeta.Nrotarjeta)))

	fmt.Printf("%s\n", resultado)

	//tarjeta3
	tarjeta = Tarjeta{5572387238460097, 3, "201708", "202208", 4559, 30000, "suspendida"}
	data, err = json.Marshal(tarjeta)
	if err != nil {
		log.Fatal(err)
	}
	CreateUpdate(db, "tarjeta", []byte(strconv.Itoa(tarjeta.Nrotarjeta)), data)
	resultado, err = ReadUnique(db, "tarjeta", []byte(strconv.Itoa(tarjeta.Nrotarjeta)))

	fmt.Printf("%s\n", resultado)
	//comercio1
	comercio := Comercio{1, "Sol de Enero", "Marquez 775", "B1644GDH", "11-5785-1548"}
	data, err = json.Marshal(comercio)
	if err != nil {
		log.Fatal(err)
	}
	CreateUpdate(db, "comercio", []byte(strconv.Itoa(comercio.Nrocomercio)), data)
	resultado, err = ReadUnique(db, "comercio", []byte(strconv.Itoa(comercio.Nrocomercio)))

	fmt.Printf("%s\n", resultado)

	//comercio2
	comercio = Comercio{2, "Palacio de la Diversion", "Inclan 3999", "C1258ABG", "11-4924-6507"}
	data, err = json.Marshal(comercio)
	if err != nil {
		log.Fatal(err)
	}
	CreateUpdate(db, "comercio", []byte(strconv.Itoa(comercio.Nrocomercio)), data)
	resultado, err = ReadUnique(db, "comercio", []byte(strconv.Itoa(comercio.Nrocomercio)))

	fmt.Printf("%s\n", resultado)
	//comercio3
	comercio = Comercio{3, "Sushi ONE", "San Martin 1302", "B1870APN", "11-7782-1327"}
	data, err = json.Marshal(comercio)
	if err != nil {
		log.Fatal(err)
	}
	CreateUpdate(db, "comercio", []byte(strconv.Itoa(comercio.Nrocomercio)), data)
	resultado, err = ReadUnique(db, "comercio", []byte(strconv.Itoa(comercio.Nrocomercio)))

	fmt.Printf("%s\n", resultado)
	//compra1
	compra := Compra{1, 4154830586255180, 1, "10-11-2021", 600, true}
	data, err = json.Marshal(compra)
	if err != nil {
		log.Fatal(err)
	}
	CreateUpdate(db, "compra", []byte(strconv.Itoa(compra.Nrooperacion)), data)
	resultado, err = ReadUnique(db, "compra", []byte(strconv.Itoa(compra.Nrooperacion)))

	fmt.Printf("%s\n", resultado)

	//compra2
	compra = Compra{2, 4074683476639311, 2, "03-04-2021", 1200, true}
	data, err = json.Marshal(compra)
	if err != nil {
		log.Fatal(err)
	}
	CreateUpdate(db, "compra", []byte(strconv.Itoa(compra.Nrooperacion)), data)
	resultado, err = ReadUnique(db, "compra", []byte(strconv.Itoa(compra.Nrooperacion)))

	fmt.Printf("%s\n", resultado)
	//compra3
	compra = Compra{3, 5572387238460097, 3, "21-01-2021", 2000, false}
	data, err = json.Marshal(compra)
	if err != nil {
		log.Fatal(err)
	}
	CreateUpdate(db, "compra", []byte(strconv.Itoa(compra.Nrooperacion)), data)
	resultado, err = ReadUnique(db, "compra", []byte(strconv.Itoa(compra.Nrooperacion)))

	fmt.Printf("%s\n", resultado)
}

