package main

import(
	"database/sql"
	_ "github.com/lib/pq"
	"log"
)
func cargarConsumos(){
		db,err := sql.Open("postgres", "user=postgres host=localhost dbname=tp1 sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	_,err = db.Exec(`
		insert into consumo values ('4154830586255180', '1912', 7, 30000.00);  --cliente 1, vigente no supera limite
		insert into consumo values ('4154830586255180', '1912', 8, 5000.00);   --cliente 1, vigente no supera limite, test alerta 1 min
		insert into consumo values ('4154830586255180', '1931', 9, 1.00);      --cliente 1, vigente no supera limite, cod seg invalido
		insert into consumo values ('5149398096322564', '4231', 14, 3000.00);  --cliente 11, vigente
		insert into consumo values ('5149398096322564', '4231', 1, 7000.00);   --cliente 11, vigente, alerta 5 min
		insert into consumo values ('5149398096322560', '4231', 1, 7000.00);   --cliente 11, vigente, no existe tarjeta
		insert into consumo values ('5957907640240048', '6795', 5, 2000.00);   --cliente 4, anulada
		insert into consumo values ('5205313550474703', '5422', 2, 1500.00);   --cliente 19, expirada
		insert into consumo values ('5165954851262681', '6558', 15, 10000.00); --cliente 14, suspendida 
		insert into consumo values ('5242390672258315', '7989', 3, 33000.00);  --cliente 20, vigente supera limite
		insert into consumo values ('5242390672258315', '7989', 8, 13000.00);  --cliente 20, vigente entra bien  			
		insert into consumo values ('5242390672258315', '7989', 8, 55000.00);  --cliente 20, vigente supera limite, tarjeta suspendida

			`)
	if err != nil {
		log.Fatal(err)
	}
}


func cargarKeys(){

	db,err := sql.Open("postgres", "user=postgres host=localhost dbname=tp1 sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	_,err = db.Exec(`

		alter table cliente  add constraint cliente_pk   primary key (nrocliente);
		alter table tarjeta  add constraint tarjeta_pk   primary key (nrotarjeta);
		alter table comercio add constraint comercio_pk  primary key (nrocomercio);
		alter table compra   add constraint compra_pk    primary key (nrooperacion);
		alter table rechazo  add constraint rechazo_pk   primary key (nrorechazo);
		alter table cierre   add constraint cierre_pk    primary key (año, mes, terminacion);
		alter table cabecera add constraint cabecera_pk  primary key (nroresumen);
		alter table detalle  add constraint detalle_pk   primary key (nroresumen, nrolinea);
		alter table alerta   add constraint alerta_pk    primary key (nroalerta);
		
		alter table tarjeta  add constraint tarjeta_nrocliente_fk  foreign key (nrocliente)  references cliente(nrocliente);
		alter table compra   add constraint compra_nrotarjeta_fk   foreign key (nrotarjeta)  references tarjeta(nrotarjeta);
		alter table compra   add constraint compra_nrocomercio_fk  foreign key (nrocomercio) references comercio(nrocomercio);
		alter table rechazo  add constraint rechazo_nrotarjeta_fk  foreign key (nrotarjeta)  references tarjeta(nrotarjeta);
		alter table rechazo  add constraint rechazo_nrocomercio_fk foreign key (nrocomercio) references comercio(nrocomercio);
		alter table cabecera add constraint cabecera_nrotarjeta_fk foreign key (nrotarjeta)  references tarjeta(nrotarjeta);
		alter table detalle  add constraint detalle_nroresumen_fk  foreign key (nroresumen)  references cabecera(nroresumen);
		alter table alerta   add constraint alerta_nrotarjeta_fk   foreign key (nrotarjeta)  references tarjeta(nrotarjeta);
		alter table alerta   add constraint alerta_nrorechazo_fk   foreign key (nrorechazo)  references rechazo(nrorechazo);

			`)
	if err != nil {
		log.Fatal(err)
	}
}


func eliminarKeys() {
		db,err := sql.Open("postgres", "user=postgres host=localhost dbname=tp1 sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	_,err = db.Exec(`
			
		alter table tarjeta  drop constraint tarjeta_nrocliente_fk;
		alter table compra   drop constraint compra_nrotarjeta_fk;
		alter table compra   drop constraint compra_nrocomercio_fk;
		alter table rechazo  drop constraint rechazo_nrotarjeta_fk;
		alter table rechazo  drop constraint rechazo_nrocomercio_fk;
		alter table cabecera drop constraint cabecera_nrotarjeta_fk;
		alter table detalle  drop constraint detalle_nroresumen_fk;
		alter table alerta   drop constraint alerta_nrotarjeta_fk;
		alter table alerta   drop constraint alerta_nrorechazo_fk;
		
		alter table cliente  drop constraint cliente_pk;
		alter table tarjeta  drop constraint tarjeta_pk;
		alter table comercio drop constraint comercio_pk;
		alter table compra   drop constraint compra_pk;
		alter table rechazo  drop constraint rechazo_pk;
		alter table cierre   drop constraint cierre_pk;
		alter table cabecera drop constraint cabecera_pk;
		alter table detalle  drop constraint detalle_pk;
		alter table alerta   drop constraint alerta_pk;

		`)
	if err != nil {
		log.Fatal(err)
	}
}


func cargarDatosClientes(){
	db,err := sql.Open("postgres", "user=postgres host=localhost dbname=tp1 sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	_,err = db.Exec(`

		insert into cliente values (1, 'Ezequiel', 'Mosqueira', 'Reconquista 1782', '11-3654-1245');
		insert into cliente values (2, 'Claudia',  'Gimenez',   'Cabildo 4058',     '11-7821-9942');
		insert into cliente values (3, 'Nicolas',  'Peralta',   'Pilcomayo 331',    '11-6548-1514');
		insert into cliente values (4, 'Juliana',  'Rivero',    'Richieri 727',     '11-3961-1679');
		insert into cliente values (5, 'Daiana',   'Roz',       'Dante 1178',       '11-1535-7855');
		insert into cliente values (6, 'Matias', 'Basilico',   'Av. San Martin 57', '11-8401-0525');
		insert into cliente values (7, 'Yamila', 'Martinez',   'Andrada 247',       '11-2973-2638');
		insert into cliente values (8, 'Adrian', 'Suazo',      'Belgrano 1754',     '11-3754-5021');
		insert into cliente values (9, 'Camila', 'Herrero',    'Lavalle 1047',      '11-4751-5927');
		insert into cliente values (10,'Martin', 'Altamira',   'Estrada 985',       '11-8950-2575');
		insert into cliente values (11, 'German',   'Costilla','Int Juan Iirigoin 2054','11-8545-6596');
		insert into cliente values (12, 'Agustina', 'Vidal',   'Av Sarmiento 1254',     '11-6632-7845');
		insert into cliente values (13, 'Facundo',  'Ruiz',    'Av Primera Junta 512',  '11-5463-2123');
		insert into cliente values (14, 'Pablo',    'Torres',  'Padre Ustarroz 112',    '11-2486-7784');
		insert into cliente values (15, 'Rocio',    'Vicente', 'Las Malvinas 336',      '11-4589-6554');
		insert into cliente values (16, 'Rin',    'Okumura',  'Chacabuco 2465',  		'11-2390-4943');
		insert into cliente values (17, 'Ichigo', 'Kurosaki', 'Einstein 2096',  	    '11-3376-5393');
		insert into cliente values (18, 'Sakura', 'Haruno',   'Centenario 1538', 		'11-2074-4603');
		insert into cliente values (19, 'Guren',  'Ichinose', 'Ayacucho 2373',   		'11-9306-7304');
		insert into cliente values (20, 'Erina',  'Nakiri',   'Drago 2759',      		'11-9940-6666');
	`)
	if err != nil {
		log.Fatal(err)
	}
}

func cargarDatosComercios() {
	db,err := sql.Open("postgres", "user=postgres host=localhost dbname=tp1 sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	_,err = db.Exec(`
		insert into comercio values (1, 'Sol de enero',            'Marquez 775',     'B1644GDH', '11-5785-1648');
		insert into comercio values (2, 'Palacio de la diversion', 'Inclan 3999',     'C1258ABG', '11-4924-6507');
		insert into comercio values (3, 'Sushi ONE',               'San Martin 1302', 'B1870APN', '11-7782-1327');
		insert into comercio values (4, 'Esperancita e hijos',     'Iguazu 221',      'B1873CFE', '11-9421-1333');
		insert into comercio values (5, 'Farmacia ABAURREA',       'Malabia 3711',    'C1414DLV', '11-8479-2052');		
		insert into comercio values (6, 'Carniceria Las marias','Cabildo 4938',   'B1667IPH', '11-5532-9927');
		insert into comercio values (7, 'Zapateria Botitas',    'Juramento 1769', 'B1646ADU', '11-6551-6935');
		insert into comercio values (8, 'Verduleria Moreno',    'Antezana 445',   'B1646ADU', '11-5128-8719');
		insert into comercio values (9, 'Supermercado Amanecer','Av.Cordoba 1455','B1667IPH', '11-2782-3355');
		insert into comercio values (10, 'Libreria Polysu',      'Moron 895',      'B1714JRF', '11-4708-3535');	
		insert into comercio values (11, 'Merceria Dany',  'Colon 456',         'B1744PAA', '11-5656-1211');
		insert into comercio values (12, 'Kevingston',     'Paunero 1545',      'B1661KOP', '11-7896-5462');
		insert into comercio values (13, 'Coto',           'Av Pte. Peron 907', 'B1663DRA', '11-1236-4568');
		insert into comercio values (14, 'Heladeria Ciwe', 'Paunero 563',       'B1744DFE', '11-2314-5695');
		insert into comercio values (15, 'Fravega',        'San jose 4563',     'B1663GFD', '11-7896-4563');			
		insert into comercio values (16, 'Anime Word',  'Cerrito 4392',    'B1613FPO', '11-7365-8770');
		insert into comercio values (17, 'Anime y Mas', 'Bacacay 7453',    'B1613FPP', '11-7365-8780');
		insert into comercio values (18, 'Otaku Fest',  'Eva Peron 6534',  'B1613FPQ', '11-7365-8889');
		insert into comercio values (19, 'AnimePlay',   'Los Robles 5242', 'B1613FNN', '11-7365-8888');
		insert into comercio values (20, 'Tio Anime',   'Zola 2023',       'B1613FFJ', '11-7365-8884');
	`)
	if err != nil {
		log.Fatal(err)
	}
}

func cargarDatosTarjetas() {

	db,err := sql.Open("postgres", "user=postgres host=localhost dbname=tp1 sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	_,err = db.Exec(`
		insert into tarjeta values ('4154830586255180', 1, '201804', '202304', '1912', 45000.00, 'vigente');
		insert into tarjeta values ('4074683476639311', 2, '201903', '202403', '1423', 30000.00, 'vigente');
		insert into tarjeta values ('5572387238460097', 3, '201708', '202208', '4559', 30000.00, 'suspendida');
		insert into tarjeta values ('3414878698317652', 3, '202007', '202507', '7048', 25000.00, 'vigente');
		insert into tarjeta values ('5957907640240048', 4, '202010', '202510', '6795', 8000.00,  'anulada');
		insert into tarjeta values ('3707576874580310', 5, '201703', '202203', '0061', 24000.00, 'suspendida');
		insert into tarjeta values ('4351220398984311', 6,  '201208', '202201', '5044', 120000.00, 'vigente');
		insert into tarjeta values ('4765829082068370', 7,  '201207', '202311', '6208', 54000.00,  'vigente');
		insert into tarjeta values ('4323245655840224', 8,  '201109', '202102', '4974', 145000.00, 'vigente');
		insert into tarjeta values ('4972337296375084', 9,  '201311', '202112', '0325', 137000.00, 'vigente');
		insert into tarjeta values ('4289795132879631', 10, '201712', '202401', '1456', 84500.00,  'vigente');
		insert into tarjeta values ('4078089085873646', 10, '201909', '202304', '7604', 254000.00, 'vigente');
		insert into tarjeta values ('5149398096322564', 11, '202103', '202403', '4231', 50000.00,  'vigente');
		insert into tarjeta values ('5382622800877589', 12, '201905', '202204', '4522', 40000.00,  'anulada');
		insert into tarjeta values ('5936590525295919', 13, '202007', '202303', '7869', 55000.00,  'vigente');
		insert into tarjeta values ('5165954851262681', 14, '201403', '201702', '6558', 60000.00,  'suspendida');
		insert into tarjeta values ('5233493395763936', 15, '202111', '202412', '4112', 120000.00, 'vigente');
		insert into tarjeta values ('5145035981969012', 16, '201906', '202406', '5344', 25000.00, 'vigente');
		insert into tarjeta values ('5546826967632223', 17, '202005', '202505', '4543', 45000.00, 'vigente');
		insert into tarjeta values ('5204653907928021', 18, '201809', '202309', '3233', 30000.00, 'vigente');
		insert into tarjeta values ('5205313550474703', 19, '201807', '201307', '5422', 3000.00,  'expirada');
		insert into tarjeta values ('5242390672258315', 20, '202012', '202512', '7989', 26000.00, 'vigente');
	`)
	
	if err != nil {
		log.Fatal(err)
	}
}

func cargarTablas() {
	cargarDatosClientes()
	cargarDatosComercios()
	cargarDatosTarjetas()
	cargarConsumos()
}
