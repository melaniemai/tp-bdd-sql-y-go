drop database if exists tp1 with(force);
create database tp1;


\c tp1

create table cliente(
    nrocliente    int,
    nombre	text,
    apellido text,
	domicilio text,
	telefono char(12)
);

create table tarjeta(
	nrotarjeta char(16),
	nrocliente int,
	validadesde char(6), --e.g. 201106
	validahasta char(6),
	codseguridad char(4),
	limitecompra decimal(8,2),
	estado char(10) --`vigente', `suspendida', `anulada'
);

create table comercio(
	nrocomercio int,
	nombre text,
	domicilio text,
	codigopostal text,
	telefono char(12)
);
create table compra(
	nrooperacion int,
	nrotarjeta char(16),
	nrocomercio int,
	fecha timestamp,
	monto decimal(7,2),
	pagado boolean
);

create table rechazo(
	nrorechazo int,
	nrotarjeta char(16),
	nrocomercio int,
	fecha timestamp,
	monto decimal(7,2),
	motivo text
);
create table cierre(
	año int,
	mes int,
	terminacion int,
	fechainicio date,
	fechacierre date,
	fechavto date
);
create table cabecera(
	nroresumen int,
	nombre text,
	apellido text,
	domicilio text,
	nrotarjeta char(16),
	desde date,
	hasta date,
	vence date,
	total decimal(8,2)
);
create table detalle(
	nroresumen int,
	nrolinea int,
	fecha date,
	nombrecomercio text,
	monto decimal(7,2)
);
create table alerta(
	nroalerta int,
	nrotarjeta char(16),
	fecha timestamp,
	nrorechazo int,
	codalerta int, --0:rechazo, 1:compra 1min, 5:compra 5min, 32:límite
	descripcion text
);
create table consumo(
	nrotarjeta char(16),
	codseguridad char(4),
	nrocomercio int,
	monto decimal(7,2)
);

alter table cliente  add constraint cliente_pk   primary key (nrocliente);
alter table tarjeta  add constraint tarjeta_pk   primary key (nrotarjeta);
alter table comercio add constraint comercio_pk  primary key (nrocomercio);
alter table compra 	 add constraint compra_pk 	 primary key (nrooperacion);
alter table rechazo  add constraint rechazo_pk 	 primary key (nrorechazo);
alter table cierre 	 add constraint cierre_pk    primary key (año, mes, terminacion);
alter table cabecera add constraint cabecera_pk  primary key (nroresumen);
alter table detalle  add constraint detalle_pk   primary key (nroresumen, nrolinea); -- nro resumen es pk de detalle??
alter table alerta 	 add constraint alerta_pk    primary key (nroalerta);

alter table tarjeta  add constraint tarjeta_nrocliente_fk  foreign key (nrocliente)  references cliente(nrocliente);
alter table compra 	 add constraint compra_nrotarjeta_fk   foreign key (nrotarjeta)  references tarjeta(nrotarjeta);
alter table compra 	 add constraint compra_nrocomercio_fk  foreign key (nrocomercio) references comercio(nrocomercio);
alter table rechazo  add constraint rechazo_nrotarjeta_fk  foreign key (nrotarjeta)  references tarjeta(nrotarjeta);
alter table rechazo  add constraint rechazo_nrocomercio_fk foreign key (nrocomercio) references comercio(nrocomercio);
alter table cabecera add constraint cabecera_nrotarjeta_fk foreign key (nrotarjeta)  references tarjeta(nrotarjeta);
alter table detalle  add constraint detalle_nroresumen_fk  foreign key (nroresumen)  references cabecera(nroresumen);
alter table alerta 	 add constraint alerta_nrotarjeta_fk   foreign key (nrotarjeta)  references tarjeta(nrotarjeta);
alter table alerta   add constraint alerta_nrorechazo_fk   foreign key (nrorechazo)  references rechazo(nrorechazo);


insert into cliente values (1, 'Ezequiel', 'Mosqueira', 'Reconquista 1782', '11-3654-1245');
insert into cliente values (2, 'Claudia',  'Gimenez',   'Cabildo 4058',     '11-7821-9942');
insert into cliente values (3, 'Nicolas',  'Peralta',   'Pilcomayo 331',    '11-6548-1514');
insert into cliente values (4, 'Juliana',  'Rivero',    'Richieri 727',     '11-3961-1679');
insert into cliente values (5, 'Daiana',   'Roz',       'Dante 1178',       '11-1535-7855');

insert into cliente values (6, 'Matias', 'Basilico', 'Av. San Martin 57','11-8401-0525');
insert into cliente values (7, 'Yamila', 'Martinez', 'Andrada 247',      '11-2973-2638');
insert into cliente values (8, 'Adrian', 'Suazo',    'Belgrano 1754',    '11-3754-5021');
insert into cliente values (9, 'Camila', 'Herrero',  'Lavalle 1047',     '11-4751-5927');
insert into cliente values (10,'Martin', 'Altamira', 'Estrada 985',      '11-8950-2575');

insert into cliente values (11, 'German',   'Costilla','Int Juan Iirigoin 2054','11-8545-6596');
insert into cliente values (12, 'Agustina', 'Vidal',   'Av Sarmiento 1254',     '11-6632-7845');
insert into cliente values (13, 'Facundo',  'Ruiz',    'Av Primera Junta',      '11-5463-2123');
insert into cliente values (14, 'Pablo',    'Torres',  'Padre Ustarroz',        '11-2486-7784');
insert into cliente values (15, 'Rocio',    'Vicente', 'Las Malvinas',          '11-4589-6554');

insert into cliente values (16, 'Rin',    'Okumura',  'Chacabuco 2465',  '11-2390-4943');
insert into cliente values (17, 'Ichigo', 'Kurosaki', 'Einstein 2096',   '11-3376-5393');
insert into cliente values (18, 'Sakura', 'Haruno',   'Centenario 1538', '11-2074-4603');
insert into cliente values (19, 'Guren',  'Ichinose', 'Ayacucho 2373',   '11-9306-7304');
insert into cliente values (20, 'Erina',  'Nakiri',   'Drago 2759',      '11-9940-6666');

insert into tarjeta values ('4154830586255180', 1, '201804', '202304', '1912', 45000.00, 'vigente');
insert into tarjeta values ('4074683476639311', 2, '201903', '202403', '1423', 30000.00, 'vigente');
insert into tarjeta values ('5572387238460097', 3, '201708', '202208', '4559', 30000.00, 'suspendida');
insert into tarjeta values ('3414878698317652', 3, '202007', '202507', '7048', 25000.00, 'vigente');
insert into tarjeta values ('5957907640240048', 4, '202010', '202510', '6795', 8000.00,  'anulada');
insert into tarjeta values ('3707576874580310', 5, '201703', '202203', '0061', 24000.00, 'suspendida');

insert into tarjeta values ('4351220398984311', 6,  '201208', '202201', '5044', 120000.00, 'vigente');
insert into tarjeta values ('4765829082068370', 7,  '201207', '202311', '6208', 54000.00,  'vigente');
insert into tarjeta values ('4323245655840224', 8,  '201109', '202102', '4974', 145000.00, 'vigente');
insert into tarjeta values ('4972337296375084', 9,  '201311', '202112', '0325', 137000.00, 'vigente');
insert into tarjeta values ('4289795132879631', 10, '201712', '202401', '1456', 84500.00,  'vigente');
insert into tarjeta values ('4078089085873646', 10, '201909', '202304', '7604', 254000.00, 'vigente');

insert into tarjeta values ('5149398096322564', 11, '202103', '202403', '4231', 50000.00,  'vigente');
insert into tarjeta values ('5382622800877589', 12, '201905', '202204', '4522', 40000.00,  'anulada');
insert into tarjeta values ('5936590525295919', 13, '202007', '202303', '7869', 55000.00,  'vigente');
insert into tarjeta values ('5165954851262681', 14, '201403', '201702', '6558', 60000.00,  'supendida');
insert into tarjeta values ('5233493395763936', 15, '202111', '202412', '4112', 120000.00, 'vigente');

insert into tarjeta values ('5145035981969012', 16, '201906', '202406', '5344', 25000.00, 'vigente');
insert into tarjeta values ('5546826967632223', 17, '202005', '202505', '4543', 45000.00, 'vigente');
insert into tarjeta values ('5204653907928021', 18, '201809', '202309', '3233', 30000.00, 'vigente');
insert into tarjeta values ('5205313550474703', 19, '201807', '201307', '5422', 3000.00,  'expirada');
insert into tarjeta values ('5242390672258315', 20, '202012', '202512', '7989', 25000.00, 'vigente');


insert into comercio values (001, 'Sol de enero',            'Marquez 775',     'B1644GDH', '11-5785-1648');
insert into comercio values (002, 'Palacio de la diversion', 'Inclan 3999',     'C1258ABG', '11-4924-6507');
insert into comercio values (003, 'Sushi ONE',               'San Martin 1302', 'B1870APN', '11-7782-1327');
insert into comercio values (004, 'Esperancita e hijos',     'Iguazu 221',      'B1873CFE', '11-9421-1333');
insert into comercio values (005, 'Farmacia ABAURREA',       'Malabia 3711',    'C1414DLV', '11-8479-2052');

insert into comercio values (006, 'Carniceria Las marias','Cabildo 4938',   'B1667IPH', '11-5532-9927');
insert into comercio values (007, 'Zapateria Botitas',    'Juramento 1769', 'B1646ADU', '11-6551-6935');
insert into comercio values (008, 'Verduleria Moreno',    'Antezana 445',   'B1661IVJ', '11-5128-8719');
insert into comercio values (009, 'Supermercado Amanecer','Av.Cordoba 1455','B1667IPH', '11-2782-3355');
insert into comercio values (010, 'Libreria Polysu',      'Moron 895',      'B1714JRF', '11-4708-3535');

insert into comercio values (011, 'Merceria Dany',  'Colon 456',         'B1744PAA', '11-5656-1211');
insert into comercio values (012, 'Kevingston',     'Paunero 1545',      'B1661KOP', '11-7896-5462');
insert into comercio values (013, 'Coto',           'Av Pte. Peron 907', 'B1663DRA', '11-1236-4568');
insert into comercio values (014, 'Heladeria Ciwe', 'Paunero 563',       'B1744DFE', '11-2314-5695');
insert into comercio values (015, 'Fravega',        'San jose 4563',     'B1663GFD', '11-7896-4563');

insert into comercio values (016, 'Anime Word',  'Cerrito 4392',    'B1613FPO', '11-7365-8770');
insert into comercio values (017, 'Anime y Mas', 'Bacacay 7453',    'B1613FPP', '11-7365-8780');
insert into comercio values (018, 'Otaku Fest',  'Eva Peron 6534',  'B1613FPQ', '11-7365-8889');
insert into comercio values (019, 'AnimePlay',   'Los Robles 5242', 'B1613FNN', '11-7365-8888');
insert into comercio values (020, 'Tio Anime',   'Zola 2023',       'B1613FFJ', '11-7365-8884');

--insert into consumo values ('4154830586255180', '1912', 003, 30000.00);--vigente no supera limite1
--insert into consumo values ('5957907640240048', '6795', 005, 2000.00); --anulada4
--insert into consumo values ('5205313550474703', '5422', 002, 1500.00); --expirada19
--insert into consumo values ('5165954851262681', '6558', 018, 12000.00); --suspendida 14
--insert into consumo values ('5242390672258315', '7989', 015, 37000.00); --vigente supera limite 20


--FUNCIONES 
create or replace function autorizacionDeCompra(ntarjeta char(16), cod char(16), numcomer int, m decimal)returns boolean as $$
declare
	t record;
	montot int;
	nrechazo int;
	ncompra int;
begin
	nrechazo := (select coalesce(max(r.nrorechazo)+1,1) from rechazo r);
	ncompra := (select coalesce(max(c.nrooperacion)+1,1) from compra c);
	select * into t from tarjeta where nrotarjeta=ntarjeta;

	if not found or  t.estado <>'vigente' then  
			
		 insert into rechazo values(nrechazo, ntarjeta, numcomer, current_date, m,
		 							'tarjeta no valida o no vigente');
		return false;
	end if;

	
	if t.codseguridad<>cod then 
		
		 insert into rechazo values(nrechazo, ntarjeta, numcomer, current_date, m,
		 							'codigo de seguridad invalido');
		return false;
	end if;
	
	select sum(monto)into montot from compra where nrotarjeta=ntarjeta;

	if (montot+m)>=t.limitecompra then
		
		 insert into rechazo values(nrechazo, ntarjeta, numcomer, current_date, m, 
		 							'supera el limite de la tarjeta');
		 return false;
	end if;

	if t.estado ='expirada' then
		
		insert into rechazo values(nrechazo, ntarjeta, numcomer, current_date, m, 
	 							'plazo de vigencia expirado');
		return false;
	end if;
	
	if t.estado ='suspendida' then
	
	 insert into rechazo values(nrechazo, ntarjeta, numcomer, current_date, m,
	 							'la tarjeta se encuentra suspendida');
	 return false;
	end if;

	insert into compra values(ncompra, ntarjeta, numcomer, current_date, m, false);
	return true;
	
end;
$$language plpgsql;


create or replace function seAgregoAlerta()returns trigger as $$
declare
	 nalerta int;
	 cantrechazos int;
	 
begin
	nalerta := (select coalesce(max(a.nroalerta)+1,1) from alerta a);
	
	insert into alerta values(nalerta, new.nrotarjeta, new.fecha, new.nrorechazo, 0, new.motivo);

	select count(new.nrotarjeta) into cantrechazos from rechazo where nrotarjeta = new.nrotarjeta
	and motivo = 'supera el limite de la tarjeta' and DATE_PART('day', fecha) = DATE_PART('day',current_date );

	if cantrechazos > 1 then
			update tarjeta set estado = 'suspendida' where nrotarjeta = new.nrotarjeta;
			insert into alerta values(nalerta,new.nrotarjeta, new.fecha, new.nrorechazo, 32, 'suspension preventiva');
	end if;
	return new;
end;	
$$language plpgsql;	


create trigger seAgregoAlerta_trg
after insert on rechazo
for each row
execute procedure seAgregoAlerta();

create or replace function comprasEnUnMinuto()returns trigger as $$
declare
	cantidad int;
	nalerta int;
	
begin
	nalerta := (select coalesce(max(a.nroalerta)+1,1) from alerta a);
	
	select count(*)into cantidad from compra where fecha>=new.fecha::TIMESTAMP - INTERVAL '1 min' and
	nrotarjeta=new.nrotarjeta and nrocomercio!=new.nrocomercio and 
	(select codigopostal from comercio c where c.nrocomercio = new.nrocomercio) = (select codigopostal from comercio c where c.nrocomercio = compra.nrocomercio);

	if cantidad>1 then -- revisar si es 0 o 1
		insert into alerta values(nalerta, new.nrotarjeta, new.fecha, 
							new.nrorechazo, 1, 'Rechazada debido a que se registro mas de una compra en un minuto ');
	end if;
	return new;
end;
$$language plpgsql;	



create trigger comprasEnUnMinuto_trg
before insert on compra
for each row
execute procedure comprasEnUnMinuto();

create or replace function comprasEnCincoMinutos() returns trigger as $$
declare 
	nalerta int;
	cantidad int;

begin

	nalerta :=( select coalesce(max(a.nroalerta)+1,1) from alerta a);
	
	select count(*)into cantidad from compra where fecha>=new.fecha::TIMESTAMP - INTERVAL '5 min' and
		nrotarjeta=new.nrotarjeta and nrocomercio!=new.nrocomercio and 
		(select codigopostal from comercio c where c.nrocomercio = new.nrocomercio) != (select codigopostal from comercio c where c.nrocomercio = compra.nrocomercio);
	
		if cantidad>1 then -- revisar si es 0 o 1
			insert into alerta values(nalerta, new.nrotarjeta, new.fecha, 
								new.nrorechazo, 5, 'Rechazada debido a que se registro mas de una compra en 5 minutos ');
		end if;
		return new;
	end;

$$language plpgsql;	

create trigger comprasEnCincoMinutos_trg
before insert on compra
for each row
execute procedure comprasEnCincoMinutos();
	



create or replace function generarResumen(numCliente int, periodo int )returns void as $$ 
declare
	cl record; 
	r int;
	nrolinea int;
	tarj record;
	cierre record; 
	ultimodigito int;
	montototal decimal(8,2);
	nombrecomercio text;
	com record;
begin
	r := (select coalesce(max(cabecera.nroresumen)+1,1) from cabecera);
	
	select * into cl from cliente where cliente.nrocliente=numCliente;
	select * into tarj from tarjeta where nrocliente=numCliente;
	select into ultimodigito right(tarj.nrotarjeta,1);
	select * into cierre from cierre where mes = periodo and terminacion = ultimodigito;
	
	nrolinea=0;
	montototal = 0;
	insert into cabecera values(r,cl.nombre,cl.apellido,cl.domicilio,tarj.nrotarjeta,cierre.fechainicio,cierre.fechacierre,cierre.fechavto,0);
	 for com in select * from  compra where nrotarjeta=tarj.nrotarjeta loop
	 	if (cierre.fechainicio <= com.fecha and cierre.fechacierre >= com.fecha and com.pagado = false) then
	 
		 	select nombre into nombrecomercio from comercio where nrocomercio=com.nrocomercio;
		 	
			insert into detalle values(r,nrolinea,com.fecha,nombrecomercio,com.monto);
			nrolinea=nrolinea+1;
			montototal = montototal + com.monto;
		end if;
		
	 end loop;

	 update cabecera set total= montototal where nroresumen=r;

end;
$$language plpgsql;


create or replace function cargarCierres() returns void as $$
declare
	i int;
begin 
	for i in 0..9 loop
		insert into cierre values (2021, generate_series(1,12), i, 
		generate_series('2021/01/01'::date+i, '2021/12/31', '1 month'),
		generate_series('2021/01/15'::date+i, '2021/12/31', '1 month'),
		generate_series('2021/01/25'::date+i, '2021/12/31', '1 month'));
	end loop;
end;
$$language plpgsql;


insert into consumo values ('4154830586255180', '1912', 003, 30000.00);--vigente no supera limite1
insert into consumo values ('5957907640240048', '6795', 005, 2000.00); --anulada4
insert into consumo values ('5205313550474703', '5422', 002, 1500.00); --expirada19
insert into consumo values ('5165954851262681', '6558', 018, 12000.00); --suspendida 14
insert into consumo values ('5242390672258315', '7989', 015, 37000.00); 
