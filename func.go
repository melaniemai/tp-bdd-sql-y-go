package main

import (
	"database/sql"
	_ "github.com/lib/pq"
	"log"
)

func crearInsertarCierres() {
	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=tp1 sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	_, err = db.Query(`
	create or replace function cargarCierres() returns void as $$
	declare
		i int;
	begin 
		for i in 0..9 loop
			insert into cierre values (2021, generate_series(1,12), i, 
			generate_series('2021/01/01'::date+i, '2021/12/31', '1 month'),
			generate_series('2021/01/15'::date+i, '2021/12/31', '1 month'),
			generate_series('2021/01/25'::date+i, '2021/12/31', '1 month'));
		end loop;
	end;
	$$language plpgsql;
	`)
	if err != nil {
		log.Fatal(err)
	}
}


func crearAutorizacionDeCompra() {
	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=tp1 sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	_, err = db.Query(`
	create or replace function autorizacionDeCompra(ntarjeta char(16), cod char(16), numcomer int, m decimal)returns boolean as $$
	declare
		t record;
		montot decimal;
		nrechazo int;
		ncompra int;
	begin
		nrechazo := (select coalesce(max(r.nrorechazo)+1, 1) from rechazo r);
		ncompra  := (select coalesce(max(c.nrooperacion)+1, 1) from compra c);
		select * into t from tarjeta where nrotarjeta=ntarjeta;
	
	
		if not found then
				insert into rechazo values(nrechazo, null, numcomer, current_date, m,
				 							'Tarjeta no valida o no vigente');
				return false;
			end if;
		
		if t.codseguridad<>cod then 
			 insert into rechazo values(nrechazo, ntarjeta, numcomer, current_date, m,
			 							'Codigo de seguridad invalido');
			return false;
		end if;
		
		select coalesce(sum(monto),0) into montot from compra where nrotarjeta=ntarjeta;
		
		if montot+m >= t.limitecompra then
			 insert into rechazo values(nrechazo, ntarjeta, numcomer, current_date, m, 
			 							'Supera el limite de la tarjeta');
			 return false;
		end if;
	
		if t.estado ='expirada' then
			insert into rechazo values(nrechazo, ntarjeta, numcomer, current_date, m, 
		 							'Plazo de vigencia expirado');
			return false;
		end if;
		
		if t.estado ='suspendida' then
			insert into rechazo values(nrechazo, ntarjeta, numcomer, current_date, m,
		 							'La tarjeta se encuentra suspendida');
		 	return false;
		end if;
		
		if t.estado='anulada' then  
			insert into rechazo values(nrechazo, ntarjeta, numcomer, current_date, m,
			 							'Tarjeta anulada');
 			 return false;
		end if;

	--SE REGISTRA LA COMPRA
		insert into compra values(ncompra, ntarjeta, numcomer, current_date, m, false);
		return true;
		
	end;
	$$language plpgsql;
	`)

	if err != nil {
		log.Fatal(err)
	}
}

func crearSeAgregoUnaAlerta() {
	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=tp1 sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	_, err = db.Query(`
	create or replace function seAgregoAlerta()returns trigger as $$
	declare
		 nalerta int;
		 cantrechazos int;
		 
	begin
		nalerta := (select coalesce(max(a.nroalerta)+1,1) from alerta a);
		
		insert into alerta values(nalerta, new.nrotarjeta, new.fecha, new.nrorechazo, 0, new.motivo);
	
		select count(new.nrotarjeta) into cantrechazos from rechazo where nrotarjeta = new.nrotarjeta
		and motivo = 'Supera el limite de la tarjeta' and DATE_PART('day', rechazo.fecha) = DATE_PART('day', current_date);
	
		if cantrechazos > 1 then
				update tarjeta set estado = 'suspendida' where nrotarjeta = new.nrotarjeta;
				insert into alerta values(nalerta+1,new.nrotarjeta, new.fecha, new.nrorechazo, 32, 'Suspension preventiva');
		end if;
		return new;
	end;	

	$$language plpgsql;	
	`)

	if err != nil {
		log.Fatal(err)
	}
}

func crearComprasTiempo() {
	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=tp1 sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	_, err = db.Query(`
	create or replace function comprasEnUnMinuto()returns trigger as $$
	declare
		cantidad int;
		nalerta int;
		
	begin
		nalerta := (select coalesce(max(a.nroalerta)+1,1) from alerta a);
		
		select count(*)into cantidad from compra where compra.fecha>=new.fecha::TIMESTAMP - INTERVAL '1 min' and
		nrotarjeta=new.nrotarjeta and nrocomercio != new.nrocomercio and 
		(select codigopostal from comercio c where c.nrocomercio = new.nrocomercio) = 
		(select codigopostal from comercio c where c.nrocomercio = compra.nrocomercio);
	
		if cantidad>0 then 
			insert into alerta values(nalerta, new.nrotarjeta, new.fecha, 
								null, 1, 'Cuidado! Se registro mas de una compra en un minuto');
		end if;
		return new;
	end;
	$$language plpgsql;	


	create or replace function comprasEnCincoMinutos() returns trigger as $$
	declare 
		nalerta int;
		cantidad int;
	
	begin
	
		nalerta := (select coalesce(max(a.nroalerta)+1, 1) from alerta a);
		
		select count(*) into cantidad from compra where compra.fecha >= new.fecha::TIMESTAMP - INTERVAL '5 min' and
			nrotarjeta = new.nrotarjeta and 
			(select codigopostal from comercio c where c.nrocomercio = new.nrocomercio) != 
			(select codigopostal from comercio c where c.nrocomercio = compra.nrocomercio);

			if cantidad > 0 then
				insert into alerta values(nalerta, new.nrotarjeta, new.fecha, 
									null, 5, 'Cuidado! Se registro mas de una compra en 5 minutos');
			end if;
			return new;
		end;
	
	$$language plpgsql;	
		`)

	if err != nil {
		log.Fatal(err)
	}
}

func crearGenerarResumen() {
	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=tp1 sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	_, err = db.Query(`

	create or replace function generarResumen(numCliente int, periodo int )returns void as $$ 
	declare
		cl record; 
		nresumen int;
		nrolinea int;
		tarj record;
		cierre record; 
		ultimodigito int;
		montototal decimal(8,2);
		nombrecomercio text;
		com record;
	begin
		nresumen := (select coalesce(max(cabecera.nroresumen)+1, 1) from cabecera);
		
		select * into cl from cliente where cliente.nrocliente=numCliente;
		select * into tarj from tarjeta where nrocliente=numCliente;
		select into ultimodigito right(tarj.nrotarjeta, 1);
		select * into cierre from cierre where mes = periodo and terminacion = ultimodigito;
		
		nrolinea=0;
		montototal = 0;
		insert into cabecera values(nresumen, cl.nombre, cl.apellido, cl.domicilio, tarj.nrotarjeta, cierre.fechainicio, 
									cierre.fechacierre, cierre.fechavto, 0);
		 for com in select * from compra where nrotarjeta = tarj.nrotarjeta loop
		 	if (cierre.fechainicio <= com.fecha and cierre.fechacierre >= com.fecha and com.pagado = false) then
		 
			 	select nombre into nombrecomercio from comercio where nrocomercio=com.nrocomercio;
			 	
				insert into detalle values(nresumen, nrolinea, com.fecha, nombrecomercio, com.monto);
				nrolinea = nrolinea + 1;
				montototal = montototal + com.monto;
			end if;
			
		 end loop;
	
		 update cabecera set total= montototal where nroresumen=nresumen;
	
	end;
	$$language plpgsql;
	`)

	if err != nil {
		log.Fatal(err)
	}

}

func crearTriggers() {
	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=tp1 sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	_, err = db.Query(`

	create trigger seAgregoAlerta_trg
	after insert on rechazo
	for each row
	execute procedure seAgregoAlerta();	

	create trigger comprasEnUnMinuto_trg
	before insert on compra
	for each row
	execute procedure comprasEnUnMinuto();

	create trigger comprasEnCincoMinutos_trg
	before insert on compra
	for each row
	execute procedure comprasEnCincoMinutos();
	`)
	
	if err != nil {
		log.Fatal(err)
	}

}
