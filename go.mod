module noSQL/noSQL

go 1.16

require (
	github.com/br0xen/boltbrowser v0.0.0-20210531150353-7f10a81cece0 // indirect
	github.com/lib/pq v1.10.4
	go.etcd.io/bbolt v1.3.6
)
